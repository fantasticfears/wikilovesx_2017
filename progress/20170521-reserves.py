import re
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle
import pprint
import copy
import redis
import pickle
import pywikibot
from bs4 import BeautifulSoup
import requests
import re
import concurrent.futures
import threading
from pywikibot import pagegenerators, WikidataBot
import webbrowser
import sys
import traceback
import time as pytime

DIR = '/Users/fantasticfears/Dev/wikilovesx/'
MAX_WORKERS = 40

zh_wikip = pywikibot.Site('zh', fam='wikipedia')
en_wikip = pywikibot.Site('en', fam='wikipedia')
wikidata = en_wikip.data_repository()
ASYNC = False

_pool = redis.ConnectionPool.from_url("redis://localhost:6379/10")
r = redis.StrictRedis(connection_pool=_pool)

from icu import Transliterator

transliterator = Transliterator.createInstance('Han-Latin; Latin-ASCII')

df = pickle.loads(r.get('df'))
en_zh_mapping = pickle.loads(r.get('en_zh_mapping'))
reserves = pickle.loads(r.get('reserves'))
translitereate_zh_district = pickle.loads(r.get('translitereate_zh_district'))


import datetime

def transliterate(zh):
    return "".join(transliterator.transliterate(zh).split(' ')).capitalize()

def resolve_response(item, exception):
    if exception:
        print("Error on: " + item)

items = {
    'natural_reserve': 'Q179049',
    'PRC': 'Q148',
    'government_doc': 'Q19828919',
    '林业': 'Q5975403', #林业部
    '农业': 'Q1050095',
    '国土': 'Q834046',
    '环保': 'Q1536450',
    '海洋': 'Q7603448',
    '住建': 'Q11366945',
    '其他': 'Q59261',
    '水利': 'Q907029',
    'gov_list': 'Q30013795',
    "square_meter": "Q25343" # miss used Q712226,
}
claims = {
    'instance_of': 'P31',
    'country': 'P17',
    'located': 'P131',
    'coordinate_location': 'P625',
    'area': 'P2046',
    'source': 'P1343',
    'started': 'P580',
    'retrieved': 'P813',
    'managed_by': 'P126',
}
level = {
    '国家级': 'National',
    '省级': 'Provincial',
    '县级': 'County level',
    '市级': 'Municipal level',
}

def run(d):
    located_mapping = pickle.loads(r.get('located_mapping'))
    pprint.pprint(d)
    zh_name = f"{d['保护区名称'][0]}{d['级别'][0]}自然保护区"
    en_name = f"{transliterate(d['保护区名称'][0])} {level[d['级别'][0]]} Natural Reserve"
    page = pywikibot.Page(zh_wikip, zh_name)
    if not page.exists():
        page = pywikibot.Page(en_wikip, f"{transliterate(d['保护区名称'][0])} {level[d['级别'][0]]} Natural Reserve")
    if page.exists():
        try:
            page = page.getRedirectTarget()
        except pywikibot.exceptions.IsNotRedirectPage:
            pass

    if page.exists():
        coordinate = page.coordinates(primary_only=True)
    else:
        coordinate = None

    try:
        if page.isDisambig():
            raise
        data_item = pywikibot.ItemPage.fromPage(page)
    except:
        data_item = pywikibot.ItemPage(wikidata)

    if data_item.exists():
        data = data_item.get()
        labels = {}
        aliases = {}
        descriptions = {}
        if data.get('labels', {}).get('zh'):
            if (data.get('labels', {}).get('zh') == zh_name or
                    data.get('labels', {}).get('zh_cn') == zh_name or
                    data.get('aliases', {}).get('zh') == zh_name):
                pass
            aliases.setdefault('zh', []).append(zh_name)
        else:
            labels['zh'] = zh_name

        if data.get('labels', {}).get('en'):
            if (data.get('labels', {}).get('en') == en_name or
                    data.get('aliases', {}).get('en') == en_name):
                pass
            aliases.setdefault('en', []).append(en_name)
        else:
            labels['en'] = en_name

        if not data.get('descriptions', {}).get('en'):
            descriptions['en'] = "A natural reserve in China."
        if not data.get('descriptions', {}).get('zh'):
            descriptions['zh'] = "中国自然保护区"
        data_item.editEntity({"aliases": aliases, "labels": labels, "descriptions": descriptions})
    else:
        data_item.editEntity({
            "labels": {
                "en": en_name,
                "zh": zh_name,
            },
            "descriptions": {
                "en": "A natural reserve in China.",
                "zh": "中国自然保护区"
            }
        }, summary="Creating item. See: [[Wikidata:WikiProject WLE PRC/Natural reserves]].")


    print(data_item)
    data_item.watch()

    # natural reserve
    instance_of = pywikibot.Claim(wikidata, claims['instance_of'])
    instance_of.setTarget(pywikibot.ItemPage(wikidata, items['natural_reserve']))

    # located

    locations = d['行政区域'][0].split('、')

    # area
    area = pywikibot.Claim(wikidata, claims['area'])
    area.setTarget(pywikibot.WbQuantity(d['面积'][0],
                                        unit=pywikibot.ItemPage(wikidata, items['square_meter']),
                                        site=wikidata))

    # source
    source = pywikibot.Claim(wikidata, claims['source'])
    source.setTarget(pywikibot.ItemPage(wikidata, items['gov_list']))

    # managed

    managed_by = pywikibot.Claim(wikidata, claims['managed_by'])
    managed_by.setTarget(pywikibot.ItemPage(wikidata, items[d['主管部'][0]]))

    data_item.addClaim(instance_of, bot=False, asynchronous=ASYNC, callback=resolve_response)
    data_item.addClaim(area, bot=False, asynchronous=ASYNC, callback=resolve_response)
    data_item.addClaim(source, bot=False, asynchronous=ASYNC, callback=resolve_response)
    data_item.addClaim(managed_by, bot=False, asynchronous=ASYNC, callback=resolve_response)

    if coordinate and data_item.claims.get(claims['coordinate_location'], None):
        cord = pywikibot.Claim(wikidata, claims['coordinate_location'])
        cord.setTarget(coordinate)
        data_item.addClaim(cord, bot=False, asynchronous=ASYNC, callback=resolve_response)

    for location in locations:
        if located_mapping.get(location):
            located = pywikibot.Claim(wikidata, claims['located'])
            located.setTarget(located_mapping[location])
            data_item.addClaim(located, bot=False, asynchronous=ASYNC, callback=resolve_response)
        else:
            print("No district information for " + location)
            try:
                print("Asking item page (press): ")
                input_ = input().strip()
                if len(input_) > 0:
                    located_mapping[location] = pywikibot.ItemPage(wikidata, input_)
                    print(f"Page loads into data: {located_mapping[location]}")
                    r.set('located_mapping', pickle.dumps(located_mapping))
                    located = pywikibot.Claim(wikidata, claims['located'])
                    located.setTarget(located_mapping[location])
                    data_item.addClaim(located, bot=False, asynchronous=ASYNC, callback=resolve_response)
            except Exception:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback.print_exception(exc_type, exc_value, exc_traceback,
                                          limit=2, file=sys.stdout)

    time = pywikibot.WbTime.fromTimestr(f"+{datetime.datetime.strptime(d['始建时间'][0], '%Y%m%d').isoformat()}Z", precision=11)
    started = pywikibot.Claim(wikidata, claims['started'])
    started.setTarget(time)
    instance_of.addQualifier(started)

    retrieved = pywikibot.Claim(wikidata, claims['retrieved'])
    date = pywikibot.WbTime(year=2017, month=5, day=22)
    retrieved.setTarget(date)
    instance_of.addQualifier(retrieved)

    url = f"https://wikidata.org/wiki/{data_item.title(asUrl=True)}"
    print(url)
    webbrowser.open(url)
    r.set('gov', pickle.dumps(gov[1:]))
    pytime.sleep(0.1)

gov = pickle.loads(r.get('gov'))
while len(gov) >= 1:
    gov = pickle.loads(r.get('gov'))
    run(gov[:1].to_dict(orient='list'))

