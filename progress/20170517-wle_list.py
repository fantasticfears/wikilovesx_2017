import pywikibot
import logging
from bs4 import BeautifulSoup
import requests
import re
import concurrent.futures
import threading
import pickle
import pandas

logging_level = logging.INFO
MAX_WORKERS = 40

DIR = '/Users/fantasticfears/Dev/wikilovesx/'
logger = logging.getLogger(__name__)
zh_wikip = pywikibot.Site('zh', fam='wikipedia')
en_wikip = pywikibot.Site('en', fam='wikipedia')
wikidata = en_wikip.data_repository()

items = {'natrual_reserve': 'Q179049',
         'PRC': 'Q148'}
claims = {'instance_of': 'P31',
          'country': 'P17',
          'located': 'P131'}


def lookup_linked_page(item):
    """returns parsed page.

    (page_name, alias)
    """
    link_pattern = re.compile('\[\[([^\]]+)\]\]')
    match = link_pattern.match(item)
    if match:
        title = match.group(1)
        if '|' in title:
            title = tuple(title.split('|'))
        else:
            title = tuple([title] * 2)
    else:
        title = None
    return title


def strip_linked_page(item):
    """returns item without link."""
    return re.sub(r'\[\[([^\|\]]+\|)?([^\]]+)\]\]', r'\2', item)


def strip_name(str_):
    """Strip a natrual reserve's name."""
    str_ = str_.strip()
    if str_.endswith('*'):
        str_ = str_[:-2]
    return str_.strip()


def expand_ul(siblings_generator):
    """Expands ul list from the siblings generator."""
    uls = []
    for s in siblings_generator:
        if s.name == None:
            pass
        elif s.name != 'ul':
            return uls
        else:
            uls.append(s)


def pickle_cache_call(cache_filename, func, **kwargs):
    """returns result from cache or invoking function (and cache them)."""
    filename = DIR + cache_filename
    try:
        with open(filename, 'rb') as f:
            result = pickle.load(f)
    except OSError:
        result = func(*tuple(value for _, value in kwargs.items()))
        with open(filename, 'wb') as f:
            pickle.dump(result, f)
    return result


def extract_natural_reserve():
    """Uploading the of natrual reserve in China to wikidata.

    The reference page is https://zh.wikipedia.org/wiki/中华人民共和国国家级自然保护区列表.
    """
    title = u'中华人民共和国国家级自然保护区列表'
    category = pywikibot.Category(zh_wikip, u'Category:中国国家级自然保护区')
    page = pywikibot.Page(zh_wikip, title)
    logger.info("Start to collect from: " + title)

    def scramble_html_for_reserves(page):
        """retruns a dict of dict of list.

        the first dict is used for area.
        the second dict is used for distinct.
        the list is used for natrual reserves.
        """
        result = {}
        url = "https:" + page.permalink()
        response = requests.get(url)
        parsed_body = BeautifulSoup(response.text, "lxml")
        h2_span_elements = [e for e in parsed_body.find_all('span', class_='mw-headline') if e.text.endswith('地区')]
        ul_elements = [expand_ul(e.parent.next_siblings) for e in h2_span_elements]
        for span, ul in zip(h2_span_elements, ul_elements):
            area = span.text  # e.g. 华北地区
            result[area] = {}
            for next_ul in ul:
                distinct = next_ul.a.text  # e.g. 北京
                names = [strip_name(a.text) for a in next_ul.ul.find_all('li')]  # e.g. [松山国家级自然保护区]
                result[area][distinct] = names
        return result

    def get_linked_pages(page):
        """returns a dict of linked page from name.

        regexp to extract from wikitext
        """
        result = {}
        item_pattern = re.compile('^\\*+(.*)$', re.MULTILINE)
        list_ = [strip_name(s) for s in item_pattern.findall(page.text) if re.match("(.*自然保护区)(\s\*)?", s)]

        def is_in_category(item):
            linked_page_title = lookup_linked_page(item)
            name = strip_linked_page(item)
            if linked_page_title is not None:
                linked_page = pywikibot.Page(zh_wikip, linked_page_title[0])
                if category in linked_page.categories():
                    return name, linked_page

        lock = threading.Lock()
        with concurrent.futures.ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
            for linked_page in executor.map(is_in_category, list_):
                if linked_page is None:
                    pass
                else:
                    lock.acquire()
                    result.update([linked_page])
                    lock.release()
        return result

    # def submit_to_wikidata(reserves, page_map):
    #     """Submit to wikidata if not present."""
    #
    #     for area, ds in reserves.items():
    #         for distinct, rs in ds:
    #             for reserve in rs:
    #                 page = page_map[reserve]
    #                 try:
    #
    #                 except (NameError, pywikibot.NoPage) as e:
    #                     item = pywikibot.ItemPage(wikidata)
    #
    #                 if page:
    #                     item = page.data_item
    #                     coordinate = page.coordinates(primary_only=True)
    #
    #                 reserve

    reserves_zh = pickle_cache_call('reserves_zh.obj', scramble_html_for_reserves, page=page)
    logger.info(reserves_zh)
    zh_linked_pages = pickle_cache_call('linked_page_zh.obj', get_linked_pages, page=page)
    logger.info(zh_linked_pages)
    # reserves_en = pickle_cache_call('reserves_en.obj', scramble_html_for_reserves, page=page)


def main():
    logging.basicConfig(level=logging_level)

    extract_natural_reserve()


if __name__ == '__main__':
    main()
