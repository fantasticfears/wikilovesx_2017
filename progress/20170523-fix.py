import re
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle
import pprint
import copy
import redis
import pickle
import pywikibot
from bs4 import BeautifulSoup
import requests
import re
import concurrent.futures
import threading
from pywikibot import pagegenerators, WikidataBot
import webbrowser
import sys
import traceback
import time as pytime
from pywikibot.pagegenerators import WikidataSPARQLPageGenerator

DIR = '/Users/fantasticfears/Dev/wikilovesx/'
MAX_WORKERS = 40
zh_wikip = pywikibot.Site('zh', fam='wikipedia')
en_wikip = pywikibot.Site('en', fam='wikipedia')
wikidata = en_wikip.data_repository()

_pool = redis.ConnectionPool.from_url("redis://localhost:6379/10")
r = redis.StrictRedis(connection_pool=_pool)

square_kilometer_items_sparql = """
SELECT ?item ?val ?unit ?nrLabel
WHERE
{
  	?item wdt:P31 wd:Q179049;
          wdt:P131 ?nrTerritorial .
          ?nrTerritorial wdt:P17 wd:Q148 .
    ?item p:P2046/psv:P2046 [
            wikibase:quantityAmount ?val;
            wikibase:quantityUnit wd:Q712226
          ].

	SERVICE wikibase:label { bd:serviceParam wikibase:language "zh" }
}"""

missing_country_items_sparql = """
SELECT ?item ?itemLabel
WHERE
{
  	?item wdt:P31 wd:Q179049;
          wdt:P131 ?nrTerritorial .
          ?nrTerritorial wdt:P17 wd:Q148 .
	FILTER NOT EXISTS {?item wdt:P17 wd:Q148}
	FILTER NOT EXISTS {?item wdt:P17 wd:Q14773}
	SERVICE wikibase:label { bd:serviceParam wikibase:language "zh" }
}"""

p_area = 'P2046'
p_country = 'P17'
q_square_meter = "Q25343"
q_prc = 'Q148'

square_kilometer_pages = set(WikidataSPARQLPageGenerator(square_kilometer_items_sparql, wikidata))
missing_country_pages = set(WikidataSPARQLPageGenerator(missing_country_items_sparql, wikidata))

# sandbox :)
# square_kilometer_pages = set([pywikibot.ItemPage(wikidata, "Q4115189")])
# missing_country_pages = set([pywikibot.ItemPage(wikidata, "Q4115189")])

square_meter_item = pywikibot.ItemPage(wikidata, q_square_meter)
prc_item = pywikibot.ItemPage(wikidata, q_prc)

def change_unit(p):
    try:
        p.get()
        for c_a in p.claims[p_area]:
            amount = c_a.getTarget().amount
            c_a.changeTarget(pywikibot.WbQuantity(amount, unit=square_meter_item, site=wikidata), bot=False)
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback, limit=2, file=sys.stdout)

[change_unit(i) for i in square_kilometer_pages]

def add_country(p):
    try:
        p.get()
        try:
            p.claims[p_country]
            return
        except KeyError:
            pass
        claim = pywikibot.Claim(wikidata, p_country)
        claim.setTarget(prc_item)
        p.addClaim(claim, bot=False)
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback, limit=2, file=sys.stdout)

[add_country(i) for i in missing_country_pages]

